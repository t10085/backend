# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_05_28_150222) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource"
  end

  create_table "admin_users", comment: "Пользователи, которые имеют доступ в панель админа", force: :cascade do |t|
    t.string "email", null: false, comment: "Почта пользователя"
    t.string "encrypted_password", null: false, comment: "Зашифрованный пароль пользователя"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
  end

  create_table "cities", comment: "Города", force: :cascade do |t|
    t.bigint "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_cities_on_region_id"
  end

  create_table "contacts", comment: "Контакты для связи с мастерами и студиями", force: :cascade do |t|
    t.bigint "contactable_id"
    t.string "contactable_type"
    t.string "kind", null: false, comment: "Тип контакта, поле enum, см. модель"
    t.string "value", null: false, comment: "Телефон, никнейм и т.д."
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contactable_id", "contactable_type"], name: "index_contacts_on_contactable_id_and_contactable_type"
  end

  create_table "countries", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "labels", comment: "Франшизы и сети студий", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", null: false, comment: "Название на оригинальном языке"
    t.string "name_translitirated", comment: "Название в английской транслитирации"
    t.jsonb "logo_data", comment: "Лого франшизы, служебное поле shrine"
  end

  create_table "masters", comment: "Тату мастера", force: :cascade do |t|
    t.bigint "city_id", comment: "Для мастеров, которые работают вне студий"
    t.bigint "metro_station_id", comment: "Для мастеров, которые работают вне студий"
    t.bigint "studio_id"
    t.string "name", null: false, comment: "Имя тату мастера на его родном языке"
    t.string "name_translitirated", comment: "Имя в английском транслите"
    t.string "address", null: false, comment: "Адрес тату мастера где он работает в случае, когда он работает не в студии"
    t.geography "coordinates", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}, comment: "Координаты расположения места, где работает мастер в случае, когда он работает не в студии"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "photo_data", comment: "Фото мастера, служебное поле shrine"
    t.index ["city_id"], name: "index_masters_on_city_id"
    t.index ["metro_station_id"], name: "index_masters_on_metro_station_id"
    t.index ["studio_id"], name: "index_masters_on_studio_id"
  end

  create_table "masters_tattoo_styles", force: :cascade do |t|
    t.bigint "master_id"
    t.bigint "tattoo_style_id"
    t.index ["master_id"], name: "index_masters_tattoo_styles_on_master_id"
    t.index ["tattoo_style_id"], name: "index_masters_tattoo_styles_on_tattoo_style_id"
  end

  create_table "metro_stations", comment: "Станции метро", force: :cascade do |t|
    t.bigint "city_id"
    t.string "name", null: false, comment: "Название"
    t.string "name_translitirated", comment: "Название в английском транслите"
    t.string "color_hex", null: false, comment: "Цвет ветки"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_metro_stations_on_city_id"
  end

  create_table "mobility_string_translations", force: :cascade do |t|
    t.string "locale", null: false
    t.string "key", null: false
    t.string "value"
    t.string "translatable_type"
    t.bigint "translatable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["translatable_id", "translatable_type", "key"], name: "index_mobility_string_translations_on_translatable_attribute"
    t.index ["translatable_id", "translatable_type", "locale", "key"], name: "index_mobility_string_translations_on_keys", unique: true
    t.index ["translatable_type", "key", "value", "locale"], name: "index_mobility_string_translations_on_query_keys"
  end

  create_table "mobility_text_translations", force: :cascade do |t|
    t.string "locale", null: false
    t.string "key", null: false
    t.text "value"
    t.string "translatable_type"
    t.bigint "translatable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["translatable_id", "translatable_type", "key"], name: "index_mobility_text_translations_on_translatable_attribute"
    t.index ["translatable_id", "translatable_type", "locale", "key"], name: "index_mobility_text_translations_on_keys", unique: true
  end

  create_table "portfolio_photos", force: :cascade do |t|
    t.bigint "owner_id"
    t.string "owner_type"
    t.jsonb "image_data", null: false, comment: "Изображение, служебное поле shrine"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "regions", comment: "Регионы, в зависимости от страны разные, в РФ - области, в США - штаты", force: :cascade do |t|
    t.bigint "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_regions_on_country_id"
  end

  create_table "studio_photos", comment: "Фотографии интерьера и экстерьера студии", force: :cascade do |t|
    t.bigint "studio_id"
    t.jsonb "image_data", null: false, comment: "Изображение, служебное поле shrine"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["studio_id"], name: "index_studio_photos_on_studio_id"
  end

  create_table "studios", comment: "Студии", force: :cascade do |t|
    t.bigint "label_id"
    t.bigint "city_id"
    t.bigint "metro_station_id"
    t.string "name", null: false, comment: "Название студии"
    t.string "name_translitirated", comment: "Название в английском транслите"
    t.string "address", null: false, comment: "Адрес студии"
    t.geography "coordinates", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}, comment: "Координаты расположения студии"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "logo_data", comment: "Лого студии, служебное поле shrine"
    t.index ["city_id"], name: "index_studios_on_city_id"
    t.index ["label_id"], name: "index_studios_on_label_id"
    t.index ["metro_station_id"], name: "index_studios_on_metro_station_id"
  end

  create_table "studios_tattoo_styles", force: :cascade do |t|
    t.bigint "studio_id"
    t.bigint "tattoo_style_id"
    t.index ["studio_id"], name: "index_studios_tattoo_styles_on_studio_id"
    t.index ["tattoo_style_id"], name: "index_studios_tattoo_styles_on_tattoo_style_id"
  end

  create_table "tattoo_styles", comment: "Стили татуировок", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "cities", "regions"
  add_foreign_key "masters", "cities"
  add_foreign_key "masters", "metro_stations"
  add_foreign_key "masters", "studios"
  add_foreign_key "masters_tattoo_styles", "masters"
  add_foreign_key "masters_tattoo_styles", "tattoo_styles"
  add_foreign_key "metro_stations", "cities"
  add_foreign_key "regions", "countries"
  add_foreign_key "studio_photos", "studios"
  add_foreign_key "studios", "cities"
  add_foreign_key "studios", "labels"
  add_foreign_key "studios", "metro_stations"
  add_foreign_key "studios_tattoo_styles", "studios"
  add_foreign_key "studios_tattoo_styles", "tattoo_styles"
end
