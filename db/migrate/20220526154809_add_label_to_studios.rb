class AddLabelToStudios < ActiveRecord::Migration[7.0]
  def change
    add_column :studios, :logo_data, :jsonb, comment: 'Лого студии, служебное поле shrine'
  end
end
