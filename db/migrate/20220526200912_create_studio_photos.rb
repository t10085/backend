class CreateStudioPhotos < ActiveRecord::Migration[7.0]
  def change
    create_table :studio_photos, comment: 'Фотографии интерьера и экстерьера студии' do |t|
      t.references :studio, foreign_key: true, index: true

      t.jsonb :image_data, null: false, comment: 'Изображение, служебное поле shrine'

      t.timestamps
    end
  end
end
