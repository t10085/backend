class CreateMetroStations < ActiveRecord::Migration[7.0]
  def change
    create_table :metro_stations, comment: 'Станции метро' do |t|
      t.references :city, foreign_key: true, index: true

      t.string :name, null: false, comment: 'Название'
      t.string :name_translitirated, comment: 'Название в английском транслите'
      t.string :color_hex, null: false, comment: 'Цвет ветки'

      t.timestamps
    end
  end
end
