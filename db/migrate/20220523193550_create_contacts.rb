class CreateContacts < ActiveRecord::Migration[7.0]
  def change
    create_table :contacts, comment: 'Контакты для связи с мастерами и студиями' do |t|
      t.bigint :contactable_id
      t.string :contactable_type

      t.string :type, null: false, comment: 'Тип контакта, поле enum, см. модель'
      t.string :value, null: false, comment: 'Телефон, никнейм и т.д.'

      t.timestamps

      t.index %i[contactable_id contactable_type]
    end
  end
end
