class RenameContactsType < ActiveRecord::Migration[7.0]
  def change
    rename_column :contacts, :type, :kind
  end
end
