class CreateCities < ActiveRecord::Migration[7.0]
  def change
    create_table :cities, comment: 'Города' do |t|
      t.references :region, foreign_key: true, index: true

      t.timestamps
    end
  end
end
