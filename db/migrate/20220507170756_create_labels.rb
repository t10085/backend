class CreateLabels < ActiveRecord::Migration[7.0]
  def change
    create_table :labels, comment: 'Франшизы и сети студий' do |t|
      t.timestamps
    end
  end
end
