class CreateMasters < ActiveRecord::Migration[7.0]
  def change
    create_table :masters, comment: 'Тату мастера' do |t|
      t.references :city, foreign_key: true, index: true, comment: 'Для мастеров, которые работают вне студий'
      t.references :metro_station, foreign_key: true, index: true, comment: 'Для мастеров, которые работают вне студий'
      t.references :studio, foreign_key: true, index: true

      t.string :name, null: false, comment: 'Имя тату мастера на его родном языке'
      t.string :name_translitirated, comment: 'Имя в английском транслите'
      t.string :address, null: false, comment: 'Адрес тату мастера где он работает в случае, когда он работает не в студии'
      t.st_point :coordinates, geographic: true, comment: 'Координаты расположения места, где работает мастер в случае, когда он работает не в студии'

      t.timestamps
    end
  end
end
