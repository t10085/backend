class CreateTattooStyles < ActiveRecord::Migration[7.0]
  def change
    create_table :tattoo_styles, comment: 'Стили татуировок' do |t|
      t.timestamps
    end
  end
end
