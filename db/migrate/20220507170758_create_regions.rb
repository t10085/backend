class CreateRegions < ActiveRecord::Migration[7.0]
  def change
    create_table :regions, comment: 'Регионы, в зависимости от страны разные, в РФ - области, в США - штаты' do |t|
      t.references :country, foreign_key: true, index: true

      t.timestamps
    end
  end
end
