class AddNameToLabels < ActiveRecord::Migration[7.0]
  def change
    add_column :labels, :name, :string, null: false, comment: 'Название на оригинальном языке'
    add_column :labels, :name_translitirated, :string, comment: 'Название в английской транслитирации'
  end
end
