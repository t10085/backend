class CreateMastersTattooStyles < ActiveRecord::Migration[7.0]
  def change
    create_table :masters_tattoo_styles do |t|
      t.references :master, foreign_key: true, index: true
      t.references :tattoo_style, foreign_key: true, index: true
    end
  end
end
