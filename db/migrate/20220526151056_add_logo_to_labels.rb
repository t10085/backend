class AddLogoToLabels < ActiveRecord::Migration[7.0]
  def change
    add_column :labels, :logo_data, :jsonb, comment: 'Лого франшизы, служебное поле shrine'
  end
end
