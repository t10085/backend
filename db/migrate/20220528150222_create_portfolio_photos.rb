class CreatePortfolioPhotos < ActiveRecord::Migration[7.0]
  def change
    create_table :portfolio_photos do |t|
      t.bigint :owner_id
      t.string :owner_type

      t.jsonb :image_data, null: false, comment: 'Изображение, служебное поле shrine'

      t.timestamps
    end
  end
end
