class AddPhotoToMasters < ActiveRecord::Migration[7.0]
  def change
    add_column :masters, :photo_data, :jsonb, comment: 'Фото мастера, служебное поле shrine'
  end
end
