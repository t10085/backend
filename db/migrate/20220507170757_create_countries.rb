class CreateCountries < ActiveRecord::Migration[7.0]
  def change
    create_table :countries, name: 'Страны' do |t|
      t.timestamps
    end
  end
end
