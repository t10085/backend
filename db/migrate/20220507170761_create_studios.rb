class CreateStudios < ActiveRecord::Migration[7.0]
  def change
    create_table :studios, comment: 'Студии' do |t|
      t.references :label, foreign_key: true, index: true
      t.references :city, foreign_key: true, index: true
      t.references :metro_station, foreign_key: true, index: true

      t.string :name, null: false, comment: 'Название студии'
      t.string :name_translitirated, comment: 'Название в английском транслите'
      t.string :address, null: false, comment: 'Адрес студии'
      t.st_point :coordinates, geographic: true, comment: 'Координаты расположения студии'

      t.timestamps
    end
  end
end
