class CreateAdminUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :admin_users, comment: 'Пользователи, которые имеют доступ в панель админа' do |t|
      t.string :email, null: false, index: { unique: true }, comment: 'Почта пользователя'
      t.string :encrypted_password, null: false, comment: 'Зашифрованный пароль пользователя'

      t.timestamps
    end
  end
end
