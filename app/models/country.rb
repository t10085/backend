class Country < ApplicationRecord
  extend Mobility

  translates :name, type: :string

  has_many :regions

  # TODO: оставил для примера, потом убрать
  # scope :name_global_cont, -> (value) {
  #   joins(:string_translations).
  #     where("mobility_string_translations.value ILIKE ?", "%#{value}%").
  #     distinct
  # }

  # def self.ransackable_scopes(auth_object = nil)
  #   %i[name_global_cont]
  # end
end
