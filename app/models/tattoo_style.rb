class TattooStyle < ApplicationRecord
  extend Mobility

  translates :name, type: :string
  translates :description, type: :text

  has_and_belongs_to_many :studios
  has_and_belongs_to_many :masters
end
