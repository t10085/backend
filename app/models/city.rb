class City < ApplicationRecord
  extend Mobility

  translates :name, type: :string

  belongs_to :region

  has_many :studios
end
