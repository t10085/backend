class Label < ApplicationRecord
  include LabelLogoUploader::Attachment(:logo)

  extend Mobility

  translates :description, type: :text

  has_many :studios

  validates :name, presence: true
end
