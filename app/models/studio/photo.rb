class Studio::Photo < ApplicationRecord
  include ImageUploader::Attachment(:image)

  self.table_name = 'studio_photos'

  belongs_to :studio

  validates :image_data, presence: true
end
