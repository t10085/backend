class Region < ApplicationRecord
  extend Mobility

  translates :name, type: :string

  belongs_to :country

  has_many :cities
end
