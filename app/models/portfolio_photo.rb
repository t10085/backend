# frozen_string_literal: true

class PortfolioPhoto < ApplicationRecord
  include ImageUploader::Attachment(:image)

  OWNER_TYPES = %w[Master Studio]

  belongs_to :owner, polymorphic: true

  validates :owner_type, inclusion: { in: OWNER_TYPES }
  validates :image_data, presence: true
end
