class Studio < ApplicationRecord
  include StudioLogoUploader::Attachment(:logo)

  extend Mobility

  translates :description, type: :text

  belongs_to :label, optional: true
  belongs_to :metro_station, optional: true
  belongs_to :city

  has_many :contacts, as: :contactable, dependent: :destroy
  has_many :masters
  has_many :studio_photos, class_name: 'Studio::Photo', dependent: :destroy
  has_many :portfolio_photos, as: :owner, dependent: :destroy

  has_and_belongs_to_many :tattoo_styles

  validates :name, presence: true

  accepts_nested_attributes_for :contacts, allow_destroy: true
  accepts_nested_attributes_for :studio_photos, allow_destroy: true
  accepts_nested_attributes_for :portfolio_photos, allow_destroy: true
end
