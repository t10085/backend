class MetroStation < ApplicationRecord
  belongs_to :city

  validates :name, presence: true
  validates :color_hex, presence: true

  has_many :studios
end
