# frozen_string_literal: true

class Contact < ApplicationRecord
  CONTACTABLE_TYPES = %w[
    Studio
    Master
  ]
  KINDS = %w[
    phone
    email
    telegram
    whatsapp
    vk
  ]

  belongs_to :contactable, polymorphic: true

  validates :contactable_type, inclusion: { in: CONTACTABLE_TYPES }
  validates :kind, inclusion: { in: KINDS }
end
