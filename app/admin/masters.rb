ActiveAdmin.register Master do
  index do
    id_column
    column :name
    column :city
    column :metro_station
    column :studio
    actions
  end

  filter :name
  filter :city
  filter :metro_station
  filter :studio

  show do
    columns do
      column do
        panel I18n.t(:general) do
          attributes_table_for master do
            row :id
            row :name
            row :name_translitirated
            row :photo do
              image_tag(master.photo_url(:small))
            end
            row :studio
            I18n.available_locales.each do |locale|
              row "#{I18n.t('activerecord.attributes.master.description')} #{locale}" do
                master.send("description_#{locale}")
              end
            end
            row :created_at
            row :updated_at
          end
        end
      end

      column do
        panel I18n.t(:geo) do
          attributes_table_for master do
            row :city
            row :metro_station
            row :address
            row :coordinates
          end
        end
        panel I18n.t('activerecord.models.contact.other') do
          table_for master.contacts do
            column :kind
            column :value
          end
        end
        panel I18n.t('activerecord.models.portfolio_photo.other') do
          div(style: 'display: flex; overflow-y: auto;') do
            master.portfolio_photos.map do |portfolio_photo|
              image_tag(portfolio_photo.image_url(:small), style: 'padding-right: 5px;')
            end.join('').html_safe
          end
        end
      end
    end
  end

  permit_params do
    result = [
      :name,
      :name_translitirated,
      :city_id,
      :metro_station_id,
      :studio_id,
      :address,
      :coordinates,
      :photo,
      contacts_attributes: [
        :id,
        :kind,
        :value,
        :_destroy,
      ],
      tattoo_style_ids: [],
      portfolio_photos_attributes: [
        :id,
        :image,
        :_destroy,
      ],
    ]
    I18n.available_locales.each do |locale|
      result << "description_#{locale}"
    end
    result
  end

  form do |f|
    columns do
      column do
        inputs I18n.t(:general) do
          input :name
          input :name_translitirated
          input :photo, as: :file, hint: image_tag(master.photo_url(:small))
          input :studio
          I18n.available_locales.each do |locale|
            input "description_#{locale}", as: :text, label: "#{I18n.t('activerecord.attributes.label.description')} #{locale}"
          end
          input :tattoo_style_ids, as: :tags, collection: TattooStyle.all, display_name: :name
        end
        actions
      end

      column do
        inputs I18n.t(:geo) do
          input :city
          input :metro_station
          input :address
          input :coordinates, as: :string, placeholder: 'POINT(0 0)'
        end
        inputs I18n.t('activerecord.models.contact.other') do
          f.has_many :contacts, allow_destroy: true do |contact_f|
            contact_f.input :kind, as: :select, collection: Contact::KINDS.map { |kind| [I18n.t(kind, scope: 'activerecord.attributes.contact.kinds'), kind] }
            contact_f.input :value
          end
        end
        inputs I18n.t('activerecord.models.portfolio_photo.other') do
          f.has_many :portfolio_photos, allow_destroy: true do |portfolio_photo_f|
            portfolio_photo_f.input :image, as: :file, hint: portfolio_photo_f.object.persisted? ? image_tag(portfolio_photo_f.object.image_url(:small)) : nil
          end
        end
      end
    end
  end
end
