ActiveAdmin.register Label do
  index do
    id_column
    column :name
    actions
  end

  filter :name

  show do |label|
    panel I18n.t(:general) do
      attributes_table_for label do
        row :id
        row :name
        row :name_translitirated
        row :logo do
          image_tag(label.logo_url(:small))
        end
        I18n.available_locales.each do |locale|
          row "#{I18n.t('activerecord.attributes.label.description')} #{locale}" do |label|
            label.send("description_#{locale}")
          end
        end
        row :created_at
        row :updated_at
      end
    end
    panel I18n.t('activerecord.models.studio.other') do
      table_for label.studios do
        column :id
        column :name do |studio|
          link_to(studio.name, admin_studio_path(studio))
        end
      end
    end
  end

  permit_params do
    result = %i[name name_translitirated logo]
    I18n.available_locales.each do |locale|
      result << "description_#{locale}"
    end
    result
  end

  form do |f|
    inputs do
      input :name
      input :name_translitirated
      input :logo, as: :file, hint: image_tag(f.object.logo_url(:small))
      I18n.available_locales.each do |locale|
        input "description_#{locale}", as: :text, label: "#{I18n.t('activerecord.attributes.label.description')} #{locale}"
      end
    end
    actions
  end
end
