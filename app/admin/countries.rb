ActiveAdmin.register Country do
  menu parent: I18n.t(:geo)

  index do
    id_column
    column :name
    actions
  end

  filter :string_translations_value_cont, label: I18n.t('activerecord.attributes.country.name')

  show do
    attributes_table do
      row :id
      I18n.available_locales.each do |locale|
        row "#{I18n.t('activerecord.attributes.country.name')} #{locale}" do
          country.send("name_#{locale}")
        end
      end
      row :created_at
      row :updated_at
    end
  end

  permit_params do
    result = []
    I18n.available_locales.each do |locale|
      result << "name_#{locale}"
    end
    result
  end

  form do |f|
    inputs do
      I18n.available_locales.each do |locale|
        input "name_#{locale}", label: "#{I18n.t('activerecord.attributes.country.name')} #{locale}"
      end
    end
    actions
  end

  action_item :regions, only: :show do
    link_to I18n.t('activerecord.models.region.other'), admin_regions_path(q: { country_id_eq: country.id })
  end
end
