ActiveAdmin.register City do
  menu parent: I18n.t(:geo)

  index do
    id_column
    column :name
    column :region
    actions
  end

  filter :string_translations_value_cont, label: I18n.t('activerecord.attributes.city.name')
  filter :region

  show do
    attributes_table do
      row :id
      I18n.available_locales.each do |locale|
        row "#{I18n.t('activerecord.attributes.city.name')} #{locale}" do
          city.send("name_#{locale}")
        end
      end
      row :region
      row :created_at
      row :updated_at
    end
  end

  permit_params do
    result = %i[region_id]
    I18n.available_locales.each do |locale|
      result << "name_#{locale}"
    end
    result
  end

  form do |f|
    inputs do
      I18n.available_locales.each do |locale|
        input "name_#{locale}", label: "#{I18n.t('activerecord.attributes.city.name')} #{locale}"
      end
      input :region
    end
    actions
  end

  action_item :metro_stations, only: :show do
    link_to I18n.t('activerecord.models.metro_station.other'), admin_metro_stations_path(q: { city_id_eq: city.id })
  end
end
