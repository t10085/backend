ActiveAdmin.register MetroStation do
  menu parent: I18n.t(:geo)

  index do
    id_column
    column :name
    column :color_hex
    column :city
    actions
  end

  filter :name
  filter :color_hex
  filter :city

  show do
    attributes_table do
      row :id
      row :name
      row :name_translitirated
      row :color_hex
      row :city
      row :created_at
      row :updated_at
    end
  end

  permit_params do
    result = []
    I18n.available_locales.each do |locale|
      result << "name_#{locale}"
    end
    result
  end

  permit_params :name, :name_translitirated, :color_hex, :city_id

  form do |f|
    inputs do
      input :name
      input :name_translitirated
      input :color_hex
      input :city
    end
    actions
  end
end
