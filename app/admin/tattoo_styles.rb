ActiveAdmin.register TattooStyle do
  index do
    id_column
    column :name
    actions
  end

  filter :string_translations_value_cont, label: I18n.t('activerecord.attributes.tattoo_style.name')

  show do
    attributes_table do
      row :id
      I18n.available_locales.each do |locale|
        row "#{I18n.t('activerecord.attributes.tattoo_style.name')} #{locale}" do
          tattoo_style.send("name_#{locale}")
        end
      end
      I18n.available_locales.each do |locale|
        row "#{I18n.t('activerecord.attributes.tattoo_style.description')} #{locale}" do
          tattoo_style.send("description_#{locale}")
        end
      end
      row :created_at
      row :updated_at
    end
  end

  permit_params do
    result = []
    I18n.available_locales.each do |locale|
      result << "name_#{locale}"
      result << "description_#{locale}"
    end
    result
  end

  form do |f|
    inputs do
      I18n.available_locales.each do |locale|
        input "name_#{locale}", label: "#{I18n.t('activerecord.attributes.tattoo_style.name')} #{locale}"
      end
      I18n.available_locales.each do |locale|
        input "description_#{locale}", as: :text, label: "#{I18n.t('activerecord.attributes.tattoo_style.description')} #{locale}"
      end
    end
    actions
  end
end
