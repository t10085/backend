ActiveAdmin.register Region do
  menu parent: I18n.t(:geo)

  index do
    id_column
    column :name
    column :country
    actions
  end

  filter :string_translations_value_cont, label: I18n.t('activerecord.attributes.region.name')
  filter :country

  show do
    attributes_table do
      row :id
      I18n.available_locales.each do |locale|
        row "#{I18n.t('activerecord.attributes.region.name')} #{locale}" do
          region.send("name_#{locale}")
        end
      end
      row :country
      row :created_at
      row :updated_at
    end
  end

  permit_params do
    result = %i[country_id]
    I18n.available_locales.each do |locale|
      result << "name_#{locale}"
    end
    result
  end

  form do |f|
    inputs do
      I18n.available_locales.each do |locale|
        input "name_#{locale}", label: "#{I18n.t('activerecord.attributes.region.name')} #{locale}"
      end
      input :country
    end
    actions
  end

  action_item :cities, only: :show do
    link_to I18n.t('activerecord.models.city.other'), admin_cities_path(q: { region_id_eq: region.id })
  end
end
