module AdminUsers
  class PasswordService
    def initialize(bcrypt_password: nil)
      @bcrypt_password = bcrypt_password || BCrypt::Password
    end

    def encrypt(password)
      @bcrypt_password.create(password)
    end

    def correct?(encrypted_password:, password:)
      return false if encrypted_password.blank? || password.blank?

      BCrypt::Password.new(encrypted_password) == password
    end
  end
end
