class LabelLogoUploader < ImageUploader
  Attacher.default_url do |**options|
    "/images/default_label_logo_#{options[:derivative] || 'medium'}.jpg"
  end
end
