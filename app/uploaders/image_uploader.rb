require 'image_processing/mini_magick'

class ImageUploader < Shrine
  Attacher.validate do
    validate_mime_type %w[image/jpeg image/jpg image/png image/webp image/tiff]
    validate_extension %w[jpg jpeg png webp tiff tif]
  end

  Attacher.derivatives do |original|
    magick = ImageProcessing::MiniMagick.source(original)

    {
      small: magick.convert('jpg').resize_to_limit!(256, 256),
      medium: magick.convert('jpg').resize_to_limit!(512, 512),
      large: magick.convert('jpg').resize_to_limit!(1024, 1024),
    }
  end
end
