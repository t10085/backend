class MasterPhotoUploader < ImageUploader
  Attacher.default_url do |**options|
    "/images/default_master_photo_#{options[:derivative] || 'medium'}.jpg"
  end
end
