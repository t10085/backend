class StudioLogoUploader < ImageUploader
  Attacher.default_url do |**options|
    "/images/default_studio_logo_#{options[:derivative] || 'medium'}.jpg"
  end
end
