# About

Backend для приложения-агрегатора различных мастеров и студий с перспективой работы в различных странах и на различных языках.

# How to start

1. install docker-compose
2. `docker-compose -f docker-compose.development.yml up`
3. open new tab in the terminal
4. install rvm
5. `rvm install 3.1.2`
6. go to the project directory
7. `rvm use`
9. `bundle`
8. `rake db:create && rake db:migrate`
10. `rails s` 
